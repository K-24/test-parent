package com.itany.utils;


import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HttpClientUtils {


    public static String doPost(String url, Map<String,String> params){

        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response=null;
        String result = "";
        try {
            HttpPost httpPost=new HttpPost(url);
            if(null!=params){
                List<NameValuePair> paramList = new ArrayList<NameValuePair>();
                for(Map.Entry<String,String> param : params.entrySet()){
                    paramList.add(new BasicNameValuePair(param.getKey()
                            ,param.getValue()));
                }

                UrlEncodedFormEntity formEntity
                        = new UrlEncodedFormEntity(paramList, Charset.defaultCharset());
                httpPost.setEntity(formEntity);

            }
            response = httpClient.execute(httpPost);
            result = EntityUtils.toString(response.getEntity(),Charset.defaultCharset());



        }catch (Exception e){
            e.printStackTrace();
        }finally {
            close(httpClient
                    ,response);
        }
        return result;
    }


    public static void close(CloseableHttpClient httpClient
            ,CloseableHttpResponse response){

        if(null!=response){
             try {
                 response.close();
             }catch (Exception e){
                 e.printStackTrace();
             }
        }
        if(null!=httpClient){
            try {
                httpClient.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }


    public static String doGet(String url, Map<String,String> params){
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response=null;
        String result = "";
        try {

            URIBuilder builder = new URIBuilder(url);
            if(null!=params){
                for(Map.Entry<String,String> param : params.entrySet()){
                    builder.addParameter(param.getKey(),param.getValue());
                }
            }

            URI uri =builder.build();
            HttpGet httpGet =new HttpGet(uri);
            response = httpClient.execute(httpGet);
            result = EntityUtils.toString(response.getEntity(),Charset.defaultCharset());

        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }





}
