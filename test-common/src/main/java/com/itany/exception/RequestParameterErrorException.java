package com.itany.exception;

/**
 * Description: 描述
 * Author: soft01
 * Date: 2019年11月07日 下午2:20
 * Version: 1.0
 */
public class RequestParameterErrorException extends Exception{
    public RequestParameterErrorException() {
    }

    public RequestParameterErrorException(String message) {
        super(message);
    }

    public RequestParameterErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public RequestParameterErrorException(Throwable cause) {
        super(cause);
    }

    public RequestParameterErrorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
