package com.itany.exception;

/**
 * Description: 描述
 * Author: soft01
 * Date: 2019年11月06日 下午5:27
 * Version: 1.0
 */
public class ManagerUserExitException extends Exception{
    public ManagerUserExitException() {
    }

    public ManagerUserExitException(String message) {
        super(message);
    }

    public ManagerUserExitException(String message, Throwable cause) {
        super(message, cause);
    }

    public ManagerUserExitException(Throwable cause) {
        super(cause);
    }

    public ManagerUserExitException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
