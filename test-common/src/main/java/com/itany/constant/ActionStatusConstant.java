package com.itany.constant;

/**
 * Description: 描述
 * Author: soft01
 * Date: 2019年11月07日 下午2:34
 * Version: 1.0
 */
public interface ActionStatusConstant {
    /**
     * 响应码为:成功
     */
    int ACTION_STATU_SUCCESS = 100;
    
    /**
     * 响应码为:失败
     */
    int ACTION_STATU_FALL = 101;
    
    /**
     响应码为:请求参数有误
     */
    int ACTION_STATU_REQUEST_PARAMETER_ERROR = 102;

    /**
     响应码为:没有权限
     */
    int ACTION_STATU_NO_PROMISSION = 103;

    /**
     响应码为:登录超时
     */
    int ACTION_STATU_LOGIN_TIME_OUT = 104;
    
}
