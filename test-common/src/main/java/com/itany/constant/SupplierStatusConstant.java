package com.itany.constant;

public interface SupplierStatusConstant {
    /**
     * 供应商状态启用
     */
    int SUPPLIER_STATUS_ENABLE = 0;

    /**
     * 供应商状态禁用
     */
    int SUPPLIER_STATUS_DISABLE = 1;
}
