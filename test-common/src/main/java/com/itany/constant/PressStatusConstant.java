package com.itany.constant;

public interface PressStatusConstant {
    /**
     * 出版社状态启用
     */
    int PRESS_STATUS_ENABLE = 0;

    /**
     * 出版社状态禁用
     */
    int PRESS_STATUS_DISABLE = 1;
}
