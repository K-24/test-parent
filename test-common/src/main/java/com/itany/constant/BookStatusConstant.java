package com.itany.constant;

/**
 * Description: 描述
 * Author: soft01
 * Date: 2019年11月12日 上午10:10
 * Version: 1.0
 */
public interface BookStatusConstant {

    /**
     * 书籍待审核
     */
    int BOOK_STATUS_WAIT_EXAMINE = -1;

    /**
     * 书籍启用
     */
    int BOOK_STATUS_SUCCESS_EXAMINE = 0;

    /**
     * 书籍禁用
     */
    int BOOK_STATUS_FAIL_EXAMINE = 1;
}
