package com.itany.constant;

/**
 * Description: 描述
 * Author: soft01
 * Date: 2019年11月12日 下午5:27
 * Version: 1.0
 */
public interface PageConstant {

    /**
     * 默认显示第一页
     */
    int DEFAULT_PAGE_NO = 1 ;

    /**
     * 默认每页显示五条数据
     */
    int DEFAULT_PAGE_SIZE = 5 ;
}
