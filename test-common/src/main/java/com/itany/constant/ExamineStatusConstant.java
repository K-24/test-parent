package com.itany.constant;

/**
 * Description: 描述
 * Author: soft01
 * Date: 2019年11月15日 下午1:45
 * Version: 1.0
 */
public interface ExamineStatusConstant {
    /**
     * 待审核
     */
    int EXAMINE_DEFAULT_WAIT = 0;


    /**
     * 审核通过
     */
    int EXAMINE_DEFAULT_SUCCESS = 1;

    /**
     * 审核未通过
     */
    int EXAMINE_DEFAULT_FAIL = -1;
}
