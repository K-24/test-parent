package com.itany.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Description: 描述
 * Author: soft01
 * Date: 2019年11月07日 下午3:35
 * Version: 1.0
 */
public class PermissionVo {
    private Integer menuid;
    private Integer parentid;
    private String icon;
    private String menuname;
    private String url;
    private List<PermissionVo> menus = new ArrayList<PermissionVo>();

    public Integer getMenuid() {
        return menuid;
    }

    public void setMenuid(Integer menuid) {
        this.menuid = menuid;
    }

    public Integer getParentid() {
        return parentid;
    }

    public void setParentid(Integer parentid) {
        this.parentid = parentid;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMenuname() {
        return menuname;
    }

    public void setMenuname(String menuname) {
        this.menuname = menuname;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<PermissionVo> getMenus() {
        return menus;
    }

    public void setMenus(List<PermissionVo> menus) {
        this.menus = menus;
    }

    @Override
    public String toString() {
        return "PermissionVo{" +
                "menuid=" + menuid +
                ", parentid=" + parentid +
                ", icon='" + icon + '\'' +
                ", menuname='" + menuname + '\'' +
                ", url='" + url + '\'' +
                ", menus=" + menus +
                '}';
    }
}


