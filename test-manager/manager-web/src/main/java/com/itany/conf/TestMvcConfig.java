package com.itany.conf;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class TestMvcConfig implements WebMvcConfigurer {

    //添加ViewController
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/showlist").setViewName("userlist");
        registry.addViewController("/showLogin").setViewName("login");
        registry.addViewController("/showbooklist").setViewName("booklist");
        registry.addViewController("/showsupplierexamine").setViewName("supplier_examine");
        registry.addViewController("/showsupplierplatform").setViewName("supplier_platform");
    }
}
