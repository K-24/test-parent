package com.itany.controller;

import com.github.pagehelper.PageInfo;
import com.itany.constant.ActionStatusConstant;
import com.itany.constant.PageConstant;
import com.itany.exception.RequestParameterErrorException;
import com.itany.pojo.Book;
import com.itany.pojo.BookWithBLOBs;
import com.itany.service.BookService;
import com.itany.utils.ParamUtil;
import com.itany.vo.ActionResult;
import com.itany.vo.BookVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * Description: 描述
 * Author: soft01
 * Date: 2019年11月07日 上午11:26
 * Version: 1.0
 */
@Controller
@RequestMapping("/book")
public class BookController {
    @Autowired
    private BookService bookService;

//    @RequestMapping("/findAll")
//    @ResponseBody
//    public Map<String,Object> findAll(@RequestParam(defaultValue = "1")Integer page,
//                                      @RequestParam(defaultValue = "5")Integer rows){
//        Map<String,Object> map = new HashMap<>();
//        PageInfo<BookVo> info = bookService.findAll(page, rows);
//        map.put("total", info.getTotal());
//        map.put("rows", info.getList());
//        return  map;
//    }


    @RequestMapping("/findAll")
    @ResponseBody
    public Map<String,Object> findAll(@RequestParam(defaultValue = "1")Integer page,
                                      @RequestParam(defaultValue = "5")Integer rows,
                                      BookVo bookVo){
        Map<String,Object> map = new HashMap<>();
        PageInfo<BookVo> info = bookService.findAll(page, rows,bookVo);
        map.put("total", info.getTotal());
        map.put("rows", info.getList());
        return  map;
    }



    @RequestMapping("/addBook")
    public ModelAndView addBook(@RequestParam(value = "file") CommonsMultipartFile file, String bookname, String isbn, String price, String number, String groupprice, String layout, String register, String weight, String bookintroduce, String authorintroduce){
        ModelAndView mav = new ModelAndView();

        try {
            bookService.addBook(file,bookname,isbn,price,number,groupprice,layout,register,weight,bookintroduce,authorintroduce);
            mav.setViewName("supplier_examine");
        } catch (Exception e) {
            e.printStackTrace();
            mav.addObject(e.getMessage());
        }

        return mav;
    }


    @RequestMapping("/modifyEnableStatus")
    @ResponseBody
    public ActionResult modifyEnableStatus(Integer id){
        ActionResult ar = new ActionResult();
        try {
            bookService.modifyEnableStatus(id);
            ar.setMsg("启用成功");
            ar.setStatus(ActionStatusConstant.ACTION_STATU_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            ar.setStatus(ActionStatusConstant.ACTION_STATU_FALL);
            ar.setMsg("服务器异常");
        }
        return ar;
    }


    @RequestMapping("/modifyDisableStatus")
    @ResponseBody
    public ActionResult modifyDisableStatus(Integer id){
        ActionResult ar = new ActionResult();
        try {
            bookService.modifyDisableStatus(id);
            ar.setMsg("禁用成功");
            ar.setStatus(ActionStatusConstant.ACTION_STATU_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            ar.setStatus(ActionStatusConstant.ACTION_STATU_FALL);
            ar.setMsg("服务器异常");
        }
        return ar;
    }


    @RequestMapping("/findById")
    @ResponseBody
    public BookWithBLOBs findById(String id){
//        ActionResult ar =new ActionResult();
//        try {
//            BookWithBLOBs book = bookService.findById(id);
//            ar.setMsg("查询成功");
//            ar.setStatus(ActionStatusConstant.ACTION_STATU_SUCCESS);
//            ar.setData(ar);
//        } catch (RequestParameterErrorException e) {
//            e.printStackTrace();
//            ar.setStatus(ActionStatusConstant.ACTION_STATU_REQUEST_PARAMETER_ERROR);
//            ar.setData(e.getMessage());
//        } catch (Exception e){
//            e.printStackTrace();
//            ar.setStatus(ActionStatusConstant.ACTION_STATU_FALL);
//            ar.setMsg("服务器内部异常");
//        }
//        return ar;
        BookWithBLOBs book = null;
        try {
            book = bookService.findById(id);
        } catch (RequestParameterErrorException e) {
            e.printStackTrace();
        }
        return book;
    }




    @RequestMapping("/modifyBook")
    public ModelAndView modifyBook(@RequestParam(value = "file",required = false)CommonsMultipartFile file, String id, String bookname,String price,String groupprice,String bookintroduce, String authorintroduce){
        ModelAndView mav = new ModelAndView();
        try {
            bookService.modifyBookById(file,id,bookname,price,groupprice,bookintroduce,authorintroduce);
            System.out.println("++++++++++++++++++++="+authorintroduce);
            mav.setViewName("redirect:/manager/showbooklist");
        } catch (Exception e) {
            e.printStackTrace();
            mav.addObject(e.getMessage());
        }
        return mav;
    }

}
