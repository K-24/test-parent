package com.itany.controller;

import com.github.pagehelper.PageInfo;
import com.itany.constant.ActionStatusConstant;
import com.itany.exception.RequestParameterErrorException;
import com.itany.pojo.Examine;
import com.itany.service.ExamineService;
import com.itany.vo.ActionResult;
import com.itany.vo.BookVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * Description: 描述
 * Author: soft01
 * Date: 2019年11月15日 上午10:52
 * Version: 1.0
 */
@Controller
@RequestMapping("/examine")
public class ExamineController {

    @Autowired
    private ExamineService examineService;

    @RequestMapping("/findAll")
    @ResponseBody
    public Map<String,Object> findAll(@RequestParam(defaultValue = "1") Integer page,
                                      @RequestParam(defaultValue = "5") Integer rows,
                                      BookVo bookVo){
        Map<String,Object> map = new HashMap<String, Object>();
        PageInfo<Examine> info = examineService.findAll(page, rows, bookVo);
        map.put("total", info.getTotal());
        map.put("rows", info.getList());
        return map;
    }
    @RequestMapping("/modifyExamine")
    @ResponseBody
    public ActionResult modifyExamine(String id,String flag, String info) {
        ActionResult ar = new ActionResult();

        try {
            examineService.modifyExamine(id, flag, info);
            ar.setMsg("修改成功");
            ar.setStatus(ActionStatusConstant.ACTION_STATU_SUCCESS);
        } catch (RequestParameterErrorException e) {
            e.printStackTrace();
            ar.setStatus(ActionStatusConstant.ACTION_STATU_REQUEST_PARAMETER_ERROR);
            ar.setMsg(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            ar.setStatus(ActionStatusConstant.ACTION_STATU_FALL);
            ar.setMsg("服务器异常");
        }
        return ar;
    }
}