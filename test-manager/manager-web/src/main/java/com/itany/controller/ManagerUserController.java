package com.itany.controller;

import com.itany.exception.ManagerUserExitException;
import com.itany.pojo.ManagerUser;
import com.itany.service.ManagerUserService;
import com.itany.service.PermissionService;
import com.itany.vo.PermissionVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Description: 描述
 * Author: soft01
 * Date: 2019年11月06日 下午5:05
 * Version: 1.0
 */
@Controller
@RequestMapping("/managerUser")
public class ManagerUserController {

    @Autowired
    private ManagerUserService managerUserService;

    @Autowired
    private PermissionService permissionService;

    @RequestMapping("/login")
    public ModelAndView login(String username, String password, HttpSession session){
        ModelAndView mav = new ModelAndView();
        try {
            ManagerUser managerUser = managerUserService.login(username, password);
            session.setAttribute("managerUser",managerUser);
            mav.setViewName("index");
        } catch (ManagerUserExitException e) {
            e.printStackTrace();
            mav.addObject("loginMsg",e.getMessage());
            mav.setViewName("login");
        } catch (Exception e){
            e.printStackTrace();
            mav.addObject("loginMsg",e.getMessage());
            mav.setViewName("login");
        }
        return mav;
    }

    @RequestMapping("/logout")
    public ModelAndView logout(HttpSession session){
        ModelAndView mav = new ModelAndView();
        session.invalidate();
        mav.setViewName("login");
        return mav;
    }

    @RequestMapping("/findPermission")
    @ResponseBody
    public List<PermissionVo> findPermission(HttpSession session){
        List<PermissionVo> list = new ArrayList<>();
        ManagerUser managerUser = (ManagerUser)session.getAttribute("managerUser");
        List<PermissionVo> permissionVoList = permissionService.findById(managerUser.getId());
        for (PermissionVo p : permissionVoList) {
                p.setIcon("icon-sys");
                for (PermissionVo p1 : p.getMenus()) {
                    p1.setIcon("icon-nav");
                }
        }
        list.addAll(permissionVoList);
        return list;
    }

}
