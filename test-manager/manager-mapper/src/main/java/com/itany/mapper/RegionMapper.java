package com.itany.mapper;

import com.itany.pojo.Region;
import com.itany.pojo.RegionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RegionMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_region
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer countByExample(RegionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_region
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer deleteByExample(RegionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_region
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_region
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer insert(Region record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_region
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer insertSelective(Region record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_region
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    List<Region> selectByExample(RegionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_region
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Region selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_region
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer updateByExampleSelective(@Param("record") Region record, @Param("example") RegionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_region
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer updateByExample(@Param("record") Region record, @Param("example") RegionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_region
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer updateByPrimaryKeySelective(Region record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_region
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer updateByPrimaryKey(Region record);
}