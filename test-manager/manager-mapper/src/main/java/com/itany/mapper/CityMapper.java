package com.itany.mapper;

import com.itany.pojo.City;
import com.itany.pojo.CityExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CityMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_city
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer countByExample(CityExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_city
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer deleteByExample(CityExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_city
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_city
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer insert(City record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_city
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer insertSelective(City record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_city
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    List<City> selectByExample(CityExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_city
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    City selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_city
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer updateByExampleSelective(@Param("record") City record, @Param("example") CityExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_city
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer updateByExample(@Param("record") City record, @Param("example") CityExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_city
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer updateByPrimaryKeySelective(City record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_city
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer updateByPrimaryKey(City record);
}