package com.itany.mapper;

import com.itany.pojo.Book;
import com.itany.pojo.BookExample;
import com.itany.pojo.BookWithBLOBs;
import java.util.List;

import com.itany.vo.BookVo;
import org.apache.ibatis.annotations.Param;

public interface BookMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_book
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer countByExample(BookExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_book
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer deleteByExample(BookExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_book
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_book
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer insert(BookWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_book
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer insertSelective(BookWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_book
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    List<BookWithBLOBs> selectByExampleWithBLOBs(BookExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_book
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    List<Book> selectByExample(BookExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_book
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    BookWithBLOBs selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_book
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer updateByExampleSelective(@Param("record") BookWithBLOBs record, @Param("example") BookExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_book
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer updateByExampleWithBLOBs(@Param("record") BookWithBLOBs record, @Param("example") BookExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_book
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer updateByExample(@Param("record") Book record, @Param("example") BookExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_book
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer updateByPrimaryKeySelective(BookWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_book
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer updateByPrimaryKeyWithBLOBs(BookWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_book
     *
     * @mbggenerated Wed Nov 06 14:17:49 CST 2019
     */
    Integer updateByPrimaryKey(Book record);



//    List<BookVo>selectAll();

    List<BookVo>selectAll(BookVo bookVo);

    void insertBook(BookWithBLOBs book);

    void modify(Book book);

    BookWithBLOBs selectById(Integer id);
}