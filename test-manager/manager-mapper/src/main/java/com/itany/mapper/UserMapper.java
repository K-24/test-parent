package com.itany.mapper;

import com.itany.pojo.User;

import java.util.List;

public interface UserMapper {
	
	public List<User> findUserAll();
	
	public void addUser(User user);
	
	public User login(User user);

}
