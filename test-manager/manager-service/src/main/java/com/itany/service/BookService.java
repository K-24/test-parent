package com.itany.service;

import com.github.pagehelper.PageInfo;
import com.itany.exception.RequestParameterErrorException;
import com.itany.pojo.Book;
import com.itany.pojo.BookWithBLOBs;
import com.itany.vo.BookVo;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * Description: 描述
 * Author: soft01
 * Date: 2019年11月07日 上午11:27
 * Version: 1.0
 */
public interface BookService {
//    PageInfo<BookVo> findAll(Integer page, Integer rows);

    PageInfo<BookVo> findAll(Integer page, Integer rows, BookVo bookVo);

    void addBook(CommonsMultipartFile file, String bookname, String isbn, String price, String number, String groupprice, String layout, String register, String weight, String bookintroduce, String authorintroduce) throws Exception;

    /**
     * 启用书籍
     * @param id
     */
    void modifyEnableStatus(Integer id);

    /**
     * 禁用书籍
     * @param id
     */
    void modifyDisableStatus(Integer id);


    /**
     * 修改书籍
     * @param file
     * @param id
     * @param bookname
     * @param price
     * @param groupprice
     * @param bookintroduce
     * @param authorintroduce
     */
    void modifyBookById(CommonsMultipartFile file, String id, String bookname,String price,String groupprice,String bookintroduce, String authorintroduce) throws Exception;



    BookWithBLOBs  findById(String id) throws RequestParameterErrorException;
}
