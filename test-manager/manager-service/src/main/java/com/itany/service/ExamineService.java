package com.itany.service;

import com.github.pagehelper.PageInfo;
import com.itany.exception.RequestParameterErrorException;
import com.itany.pojo.Examine;
import com.itany.vo.BookVo;

/**
 * Description: 描述
 * Author: soft01
 * Date: 2019年11月15日 上午10:53
 * Version: 1.0
 */
public interface ExamineService {
    PageInfo<Examine> findAll(Integer page, Integer rows, BookVo bookVo);

    void addExamine(Examine examine);

    void modifyExamine(String id, String flag, String info) throws RequestParameterErrorException;
}
