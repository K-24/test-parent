package com.itany.service;

import com.itany.vo.PermissionVo;

import java.util.List;

/**
 * Description: 描述
 * Author: soft01
 * Date: 2019年11月07日 下午2:26
 * Version: 1.0
 */
public interface PermissionService {


    List<PermissionVo> findById(Integer id);
}
