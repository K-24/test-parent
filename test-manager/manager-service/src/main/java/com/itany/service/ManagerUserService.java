package com.itany.service;

import com.itany.exception.ManagerUserExitException;
import com.itany.pojo.ManagerUser;

/**
 * Description: 描述
 * Author: soft01
 * Date: 2019年11月06日 下午5:09
 * Version: 1.0
 */
public interface ManagerUserService {
    /**
     * 后台账号登录
     * @param username
     * @param password
     * @return
     * @throws ManagerUserExitException
     */
    ManagerUser login(String username, String password) throws ManagerUserExitException;
}
