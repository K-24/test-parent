package com.itany.book.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itany.constant.ExamineStatusConstant;
import com.itany.exception.RequestParameterErrorException;
import com.itany.mapper.ExamineMapper;
import com.itany.pojo.Examine;
import com.itany.service.ExamineService;
import com.itany.utils.ParamUtil;
import com.itany.vo.BookVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Description: 描述
 * Author: soft01
 * Date: 2019年11月15日 上午10:54
 * Version: 1.0
 */

@Service
@Transactional(propagation = Propagation.SUPPORTS,rollbackFor = Exception.class)
public class ExamineServiceImpl implements ExamineService {
    @Autowired
    private ExamineMapper examineMapper;

    @Override
    public PageInfo<Examine> findAll(Integer page, Integer rows, BookVo bookVo) {
        PageHelper.startPage(page,rows);
        List<Examine> examines = examineMapper.selectAll(bookVo);
        PageInfo<Examine> info = new PageInfo<Examine>(examines);
        return info;
    }

    @Override
    public void addExamine(Examine examine) {
        examineMapper.insertSelective(examine);
    }

    @Override
    public void modifyExamine(String id, String flag, String info) throws RequestParameterErrorException {
        if (ParamUtil.isNull(id) || ParamUtil.isNull(flag) || ParamUtil.isNull(info)){
            throw new RequestParameterErrorException("请求参数有误");
        }
        Examine examine = new Examine();
        examine.setId(Integer.parseInt(id));
        examine.setFlag(Integer.parseInt(flag));
        examine.setInfo(info);
        examineMapper.updateByPrimaryKey(examine);

    }
}
