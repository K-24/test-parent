package com.itany.book.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itany.constant.ExamineStatusConstant;
import com.itany.exception.RequestParameterErrorException;
import com.itany.mapper.BookMapper;
import com.itany.mapper.ExamineMapper;
import com.itany.pojo.Book;
import com.itany.pojo.BookExample;
import com.itany.pojo.BookWithBLOBs;
import com.itany.pojo.Examine;
import com.itany.service.BookService;
import com.itany.utils.ParamUtil;
import com.itany.utils.SFTPUtils;
import com.itany.vo.BookVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Description: 描述
 * Author: soft01
 * Date: 2019年11月07日 上午11:27
 * Version: 1.0
 */
@Service
@Transactional(rollbackFor = Exception.class,propagation = Propagation.SUPPORTS)
public class BookServiceImpl implements BookService {
    @Autowired
    private BookMapper bookMapper;

    @Autowired
    private ExamineMapper examineMapper;

//
//    @Override
//    @Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
//    public PageInfo<BookVo> findAll(Integer page, Integer rows) {
//        PageHelper.startPage(page,rows);
//        List<BookVo> list = bookMapper.selectAll();
//        PageInfo<BookVo> info = new PageInfo<BookVo>(list);
//        return info;
//    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
    public PageInfo<BookVo> findAll(Integer page, Integer rows, BookVo bookVo) {
        PageHelper.startPage(page,rows);
        List<BookVo> list = bookMapper.selectAll(bookVo);
        PageInfo<BookVo> info = new PageInfo<BookVo>(list);
        return info;
    }



    @Override
    public void addBook(CommonsMultipartFile file, String bookname, String isbn, String price, String number, String groupprice, String layout, String register, String weight, String bookintroduce, String authorintroduce) throws Exception {
        BookWithBLOBs book = new BookWithBLOBs();
        if (ParamUtil.isNull(bookname) || ParamUtil.isNull(isbn) || ParamUtil.isNull(price) || ParamUtil.isNull(number) || ParamUtil.isNull(groupprice) || ParamUtil.isNull(layout) || ParamUtil.isNull(register) ||
                ParamUtil.isNull(weight) || ParamUtil.isNull(bookintroduce) || ParamUtil.isNull(authorintroduce)){
            throw new RequestParameterErrorException("请求参数有误");
        }
        book.setBookname(bookname);
        book.setIsbn(isbn);
        book.setPrice(Double.parseDouble(price));
        book.setNumber(Integer.parseInt(number));
        book.setGroupprice(Double.parseDouble(groupprice));
        book.setLayout(Integer.parseInt(layout));
        book.setRegister(register);
        book.setWeight(weight);
        book.setBookintroduce(bookintroduce);
        book.setAuthorintroduce(authorintroduce);
        book.setFlag(-1);
        book.setPressid(3);
        book.setCreatetime(new Date());

        String originalFilename = file.getOriginalFilename();
        String s = SFTPUtils.sftpPut("192.168.6.8", "tyh", "123",
                22, "yaojunjie", file.getInputStream(), originalFilename);
        String url = "/yaojunjie" + s;

        book.setImgurl(url);
        bookMapper.insertBook(book);

        Examine examine = new Examine();
        examine.setBookid(book.getId());
        examine.setTitle(bookname);
        examine.setInfo(bookname+"待审核");
        examine.setFlag(ExamineStatusConstant.EXAMINE_DEFAULT_WAIT);
        examine.setNumber(100);
        examineMapper.insertSelective(examine);
    }

    @Override
    public void modifyEnableStatus(Integer id) {
        BookWithBLOBs book = new BookWithBLOBs();
        book.setId(id);
        book.setFlag(0);
        bookMapper.modify(book);
//        bookMapper.updateByPrimaryKeySelective(book);
    }

    @Override
    public void modifyDisableStatus(Integer id) {
        BookWithBLOBs book = new BookWithBLOBs();
        book.setId(id);
        book.setFlag(1);
        bookMapper.modify(book);
    }

    @Override
    public void modifyBookById(CommonsMultipartFile file, String id, String bookname, String price, String groupprice, String bookintroduce, String authorintroduce) throws Exception {
        if (ParamUtil.isNull(id) ||  ParamUtil.isNull(bookname) ||  ParamUtil.isNull(price) ||  ParamUtil.isNull(groupprice) ||
                ParamUtil.isNull(bookintroduce) ||  ParamUtil.isNull(authorintroduce) ){
            throw new RequestParameterErrorException("请求参数有误");
        }
        BookWithBLOBs book = new BookWithBLOBs();
        book.setId(Integer.parseInt(id));
        book.setBookname(bookname);
        book.setPrice(Double.parseDouble(price));
        book.setGroupprice(Double.parseDouble(groupprice));
        book.setBookintroduce(bookintroduce);
        book.setAuthorintroduce(authorintroduce);


        if (!file.isEmpty()){
            String s = SFTPUtils.sftpPut(
                    "192.168.6.8", "tyh", "123",
                    22, "yaojunjie", file.getInputStream(), file.getOriginalFilename());
            String url = "/yaojunjie" + s;

            book.setImgurl(url);
        }
        bookMapper.modify(book);

    }

    @Override
    public BookWithBLOBs findById(String id) throws RequestParameterErrorException {
        if (ParamUtil.isNull(id)){
            throw new RequestParameterErrorException("请求参数有误");
        }
        BookWithBLOBs book = bookMapper.selectById(Integer.parseInt(id));
        return book;
    }
}
