package com.itany.book.service.impl;

import com.itany.exception.ManagerUserExitException;
import com.itany.exception.RequestParameterErrorException;
import com.itany.mapper.ManagerUserMapper;
import com.itany.pojo.ManagerUser;
import com.itany.pojo.ManagerUserExample;
import com.itany.service.ManagerUserService;
import com.itany.utils.ParamUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Description: 描述
 * Author: soft01
 * Date: 2019年11月06日 下午5:09
 * Version: 1.0
 */
@Service
@Transactional(rollbackFor = Exception.class,propagation = Propagation.SUPPORTS)
public class ManagerUserServiceImpl implements ManagerUserService {
    @Autowired
    private ManagerUserMapper managerUserMapper;

    /**
     * 后台账号登录
     * @param username
     * @param password
     * @return
     * @throws ManagerUserExitException
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public ManagerUser login(String username, String password) throws ManagerUserExitException {
        if (ParamUtil.isNull(username)){
            throw new ManagerUserExitException("账号不能为空");
        }
        if (ParamUtil.isNull(password)){
            throw new ManagerUserExitException("密码不能为空");
        }
        ManagerUser managerUser = managerUserMapper.selectByUsername(username);
        if (null == managerUser){
            throw new ManagerUserExitException("用户不存在");
        }
        if (!password.equals(managerUser.getPassword())){
            throw new ManagerUserExitException("密码错误");
        }
        return managerUser;
    }
}
