package com.itany.book.service.impl;

import com.itany.mapper.SupplierMapper;
import com.itany.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Description: 描述
 * Author: soft01
 * Date: 2019年11月14日 上午9:08
 * Version: 1.0
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS,rollbackFor = Exception.class)
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierMapper supplierMapper;
}
