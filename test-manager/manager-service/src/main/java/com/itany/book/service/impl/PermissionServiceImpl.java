package com.itany.book.service.impl;

import com.itany.mapper.PermissionMapper;
import com.itany.service.PermissionService;
import com.itany.vo.PermissionVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/**
 * Description: 描述
 * Author: soft01
 * Date: 2019年11月07日 下午2:27
 * Version: 1.0
 */
@Service
@Transactional(rollbackFor = Exception.class,propagation = Propagation.SUPPORTS)
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    @Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
    public List<PermissionVo> findById(Integer id) {
        List<PermissionVo> list = permissionMapper.selectById(id);
        return list;
    }

}
